add framwork

~~~
hw/Kconfig          
hw/arm/Kconfig      
hw/kenny/Kconfig    
hw/kenny/ghms.c     
hw/kenny/meson.build
hw/meson.build      
~~~





## 数据结构

~~~
struct TypeInfo
{
    const char *name;
    const char *parent;

    size_t instance_size;
    void (*instance_init)(Object *obj);
    void (*instance_post_init)(Object *obj);
    void (*instance_finalize)(Object *obj);

    bool abstract;
    size_t class_size;

    void (*class_init)(ObjectClass *klass, void *data);
    void (*class_base_init)(ObjectClass *klass, void *data);
    void *class_data;

    InterfaceInfo *interfaces;
};
~~~

~~~
#define TYPE_DEVICE "device"
#define DEVICE(obj) OBJECT_CHECK(DeviceState, (obj), TYPE_DEVICE)
#define DEVICE_CLASS(klass) OBJECT_CLASS_CHECK(DeviceClass, (klass), TYPE_DEVICE)
#define DEVICE_GET_CLASS(obj) OBJECT_GET_CLASS(DeviceClass, (obj), TYPE_DEVICE)
~~~

~~~
/**
 * OBJECT_CLASS_CHECK:
 * @class_type: The C type to use for the return value.
 * @class: A derivative class of @class_type to cast.
 * @name: the QOM typename of @class_type.
 *
 * A type safe version of @object_class_dynamic_cast_assert.  This macro is
 * typically wrapped by each type to perform type safe casts of a class to a
 * specific class type.
 */
#define OBJECT_CLASS_CHECK(class_type, class, name) \
    ((class_type *)object_class_dynamic_cast_assert(OBJECT_CLASS(class), (name), \
                                               __FILE__, __LINE__, __func__))
                                               
  /**
 * OBJECT_CLASS:
 * @class: A derivative of #ObjectClass.
 *
 * Converts a class to an #ObjectClass.  Since all objects are #Objects,
 * this function will always succeed.
 */
#define OBJECT_CLASS(class) \
    ((ObjectClass *)(class))
  
  
  DeviceClass *dc = DEVICE_CLASS(oc)
~~~

type_init 类似kernel module_init技术，将一组函数指针放到一个数组中。

~~~
type_init(ghms_register_types) 
~~~

将描述一种类型的对象ghms_pci注册到数组中。

~~~
type_register_static(&ghms_pci)
~~~

描述新子类，类似C++的子类，包括子类的名字、父类名字、数据内存大小（前面是父类的数据大小，后面是子类本身的数据大小）、类实例初始化函数（类似

C++中的构造函数吧）、类型的初始化函数、一组接口

~~~
static const TypeInfo ghms_pci = {
    .name          = GHMS_PCI_TYPE_NAME,
    .parent        = TYPE_PCI_DEVICE,
    .instance_size = sizeof(GhmsPciState),
    .instance_init = ghms_pci_init,
    .class_init    = ghms_class_init,
    .interfaces    = ghms_pci_if
};
~~~

类的数据成员，包含父类和当前类两部分。

~~~
typedef struct GhmsRawState {
    MemoryRegion iomem;
    DevMMUState *devmmu;

    qemu_irq irq;
    QEMUTimer *timer;
    dma_addr_t sm_dma;
    bool is_pci;
    Object *obj;

    /* properties */
    uint64_t update_times;
    uint64_t sync_data;
    uint32_t buf_size;
    bool pcie_ats_mode; /* use ats to access target memory */
    bool pcie_prg_mode;
    struct ghms_channel channels[GHMS_CHANNEL_NUM];
    struct ghms_atc atc[GHMS_ATC_DEPTH];
    /* get value from ats cap stu field */
} GhmsRawState;

typedef struct GhmsPciState {
    PCIDevice parent;
    GhmsRawState raw;
} GhmsPciState;
~~~



### 类的对象创建

~~~
Object *object_new(const char *typename)
{
    TypeImpl *ti = type_get_by_name(typename); // 根据类型名，查找类型实例TypeImpl（与TypeInfo大部分内容一致）

    return object_new_with_type(ti);
}
~~~

~~~
static Object *object_new_with_type(Type type)
{
    Object *obj;

    g_assert(type != NULL);
    type_initialize(type);  //初始化类型（如ghms_pci和父类），会调用class_init

    obj = g_malloc(type->instance_size);
    object_initialize_with_type(obj, type->instance_size, type);//初始化类的对象（如ghms_pci xx和父类对象），会调用instance_init
    obj->free = g_free;

    return obj;
}
~~~

class_init 函数初始化属于父类的数据成员（类似于父类的构造函数），同时初始化子类本身初始化函数指针realize（类似子类的构造函数）。

~~~
static void ghms_class_init(ObjectClass *oc, void *data) {
    DeviceClass *dc = DEVICE_CLASS(oc);

    //KENNY_DEBUG("register ghms class %d\n", ghms_is_pci(oc));

    if (ghms_is_pci(oc)) {
        PCIDeviceClass *k = PCI_DEVICE_CLASS(oc);
        k->realize = ghms_pci_realize;
        k->exit = ghms_pci_uninit;
        k->vendor_id = PCI_VENDOR_ID_QEMU;
        k->device_id = 0x3000; //todo: select carefully
        k->revision = 0x10;
        k->class_id = PCI_CLASS_OTHERS;
        set_bit(DEVICE_CATEGORY_MISC, dc->categories);
    }else {
        dc->realize = ghms_realize;
        dc->unrealize = ghms_unrealize;
    }
    dc->reset = ghms_reset;

}
~~~

初始化子类本身的数据成员。

~~~
static void ghms_pci_realize(PCIDevice *pdev, Error **errp)
{
    GhmsPciState *s = GHMS_PCI(pdev);
    PCIBus *bus;
    uint8_t *pci_conf = pdev->config;
    Error *err = NULL;
    uint16_t pci_cmd;

    /* set it as master. but this mean very little,
     * because vfio-pci driver will remove it
     */
    pci_cmd = pci_get_word(pdev->config + PCI_COMMAND);
    pci_cmd |= PCI_COMMAND_MASTER;
    pci_set_word(pdev->config + PCI_COMMAND, pci_cmd);

    pci_config_set_interrupt_pin(pci_conf, 1);

    /* add a vector to msi vectors */
    if (use_msi && msi_init(pdev, 0, 1, true, false, &err)) {
        KENNY_DEBUG("msi_init fail\n");
        error_append_hint(&err, "ghms msi init fail\n");
        error_propagate(errp, err);
    }
    error_free(err);

    pci_register_bar(pdev, 0, PCI_BASE_ADDRESS_SPACE_MEMORY, &s->raw.iomem);

#define GHMS_PCI_EXPRESS_CAP_OFFSET         0xe0
#define GHMS_PCI_PASID_CAP_OFFSET           0x100
#define GHMS_PCI_ATS_CAP_OFFSET             0x120
#define GHMS_PCI_PRI_CAP_OFFSET             0x140
    bus = pci_get_bus(pdev);
    if (pci_bus_is_express(bus)) {
        pcie_endpoint_cap_init(pdev, GHMS_PCI_EXPRESS_CAP_OFFSET);
        pcie_pasid_init(pdev, GHMS_PCI_PASID_CAP_OFFSET);
        pcie_ats_init(pdev, GHMS_PCI_ATS_CAP_OFFSET);
        pcie_pri_init(pdev, GHMS_PCI_PRI_CAP_OFFSET);
        pcie_set_pri_handler(pdev, ghms_pri_handler, &s->raw);
        pcie_set_atc_invalidate(pdev, ghms_atc_invalidate, &s->raw);
    } else
        KENNY_DEBUG("ghms is not connect to pcie bus, some feature disabled\n");

    /* device property is got after instance init, so use it in realize */
    ghms_internal_buf_init(&s->raw);
}
~~~

当qemu启动参数中有“-device ghms_pci”，启动后就会创建ghms_pci的实例。



## 设备驱动

目录：hw/

~~~
type_init(ghms_register_types)
~~~



## 创建设备



~~~

~~~







附录：

启动参数

~~~
#!/bin/sh

#if use drive which if=ide, set root to /dev/sda1
#if use drive which if=virtio, set root to /dev/vda1

P9PATH=p9root
P9="-fsdev local,id=p9fs,path=$P9PATH,security_model=mapped \
        -device virtio-9p-pci,fsdev=p9fs,mount_tag=p9"
NET="-netdev type=tap,id=t1 -device virtio-net-pci,netdev=t1"

../qemu-mainline/build/riscv64-softmmu/qemu-system-riscv64 \
        -s \
        -smp 8 -m 1024m \
        -nographic \
        -machine virt \
        -kernel arch/riscv/boot/Image \
        -append "root=/dev/vda ro console=ttyS0" \
        -drive file=busybear.bin,format=raw,id=hd0 \
        -device bbox-proxy -device ghms_pci \
        -device virtio-blk-device,drive=hd0

~~~





